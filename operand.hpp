//
// operand.hpp for operand in /home/amstuta/rendu/abstractVM
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Mon Feb  9 13:01:54 2015 arthur
// Last update Fri Feb 27 11:57:45 2015 raphael elkaim
//

#ifndef OPERAND_HPP_
#define OPERAND_HPP_

#include <typeinfo>
#include <sstream>
#include <cmath>
#include <limits>
#include "abstractVM.hpp"
#include "IOperand.hpp"
#include "error.hpp"

template <typename T>
class Operand: public IOperand
{
  T	value;
  eOperandType type;
  
public:
  Operand(const T op=0, eOperandType ty = INT8):
    value(op), type(ty)
  {
  }
  
  virtual ~Operand()
  {
  }
  
  std::string const	&toString() const
  {
    std::ostringstream   	res;

    if (typeid(T) == typeid(int8_t))
      res << static_cast<int>(value);
    else
      res << value;
    return *(new std::string(res.str()));
  }
  
  int			getPrecision() const
  {
    return sizeof(T);
  }
  
  eOperandType		getType() const
  {
    return type;
  }
  
  IOperand		*operator+(const IOperand &rhs) const
  {
    eOperandType	resType;
    eOperandType	type1 = getType();
    eOperandType	type2 = rhs.getType();
    std::istringstream	slhs(toString());
    std::istringstream	srhs(rhs.toString());	
	
    (type1 > type2) ? (resType = type1) : (resType = type2);
    try
      {
	switch (resType)
	  {
	  case INT8:
	    int	vlhs8;
	    int	vrhs8;
	    
	    slhs >> vlhs8;
	    srhs >> vrhs8;
	    if ((vrhs8 < 0) == (vlhs8 < 0))
	      {
		if (vrhs8 > std::numeric_limits<char>::max() - vlhs8)
		  throw Error(-1, errors[NOVERFLOW]);
		if (vrhs8 < std::numeric_limits<char>::min() + (vlhs8 < 0? -1 * vlhs8 : vlhs8))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<int8_t>(static_cast<int8_t>(vrhs8 + vlhs8), INT8));
	    
	  case INT16:
	    short	vlhs16;
	    short	vrhs16;
	    
	    slhs >> vlhs16;
	    srhs >> vrhs16;
	    if ((vrhs16 < 0) == (vlhs16 < 0))
	      {
		if (vrhs16 > std::numeric_limits<short>::max() - vlhs16)
		  throw Error(-1, errors[NOVERFLOW]);
		if (vrhs16 < std::numeric_limits<short>::min() + (vlhs16 < 0? -1 * vlhs16 : vlhs16))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<int16_t>(vrhs16 + vlhs16, INT16));
	    
	  case INT32:
	    int	vlhs32;
	    int	vrhs32;
	    
	    slhs >> vlhs32;
	    srhs >> vrhs32;
	    if ((vrhs32 < 0) == (vlhs32 < 0))
	      {
		if (vrhs32 > std::numeric_limits<int>::max() - vlhs32)
		  throw Error(-1, errors[NOVERFLOW]);
		if (vrhs32 < std::numeric_limits<int>::min() + (vlhs32 < 0? -1 * vlhs32 : vlhs32))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<int32_t>(vrhs32 + vlhs32, INT32));
	    
	  case FLOAT:
	    float	vlhsf;
	    float	vrhsf;
	    
	    slhs >> vlhsf;
	    srhs >> vrhsf;
	    if ((vrhsf < 0) == (vlhsf < 0))
	      {
		if (vrhsf > std::numeric_limits<short>::max() - vlhsf)
		  throw Error(-1, errors[NOVERFLOW]);
		if (vrhsf < std::numeric_limits<short>::min() + (vlhsf < 0? -1 * vlhsf : vlhsf))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<float>(vrhsf + vlhsf, FLOAT));
	    
	  case DOUBLE:
	    double	vlhsd;
	    double	vrhsd;
	    
	    slhs >> vlhsd;
	    srhs >> vrhsd;
	    if ((vrhsd < 0) == (vlhsd < 0))
	      {
		if (vrhsd > std::numeric_limits<int>::max() - vlhsd)
		  throw Error(-1, errors[NOVERFLOW]);
		if (vrhsd < std::numeric_limits<int>::min() + (vlhsd < 0? -1 * vlhsd : vlhsd))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<double>(vrhsd + vlhsd, DOUBLE));
	  };
      }
    catch (Error e)
      {
	std::cerr << e.what() << std::endl;
	std::exit(EXIT_FAILURE);
      }
    return 0;
  }
  
  IOperand		*operator-(const IOperand &rhs) const
  {
    eOperandType	resType;
    eOperandType	type1 = getType();
    eOperandType	type2 = rhs.getType();
    std::istringstream	slhs(toString());
    std::istringstream	srhs(rhs.toString());
    
    type1 > type2 ? resType = type1 : resType = type2;
    try
      {
	switch (resType)
	  {
	  case INT8:
	    int	vlhs8;
	    int	vrhs8;
	    
	    slhs >> vlhs8;
	    srhs >> vrhs8;
	    if ((vlhs8 >= 0 && vrhs8 <= 0) || (vlhs8 <= 0 && vrhs8 >= 0))
	      {
		if (vlhs8 > std::numeric_limits<char>::max() - ABS(vrhs8))
		  throw Error(-1, errors[NOVERFLOW]);
		if (vlhs8 < std::numeric_limits<char>::min() + ABS(vrhs8))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<int8_t>(static_cast<int8_t>(vlhs8 - vrhs8), INT8));
	    
	  case INT16:
	    short	vlhs16;
	    short	vrhs16;
	    
	    slhs >> vlhs16;
	    srhs >> vrhs16;
	    if ((vlhs16 >= 0 && vrhs16 <= 0) || (vlhs16 <= 0 && vrhs16 >= 0))
	      {
		if (vlhs16 > std::numeric_limits<short>::max() - ABS(vrhs16))
		  throw Error(-1, errors[NOVERFLOW]);
		if (vlhs16 < std::numeric_limits<short>::min() + ABS(vrhs16))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<int16_t>(vlhs16 - vrhs16, INT16));
	    
	  case INT32:
	    int	vlhs32;
	    int	vrhs32;
	    
	    slhs >> vlhs32;
	    srhs >> vrhs32;
	    if ((vlhs32 >= 0 && vrhs32 <= 0) || (vlhs32 <= 0 && vrhs32 >= 0))
	      {
		if (vlhs32 > std::numeric_limits<int>::max() - ABS(vrhs32))
		  throw Error(-1, errors[NOVERFLOW]);
		if (vlhs32 < std::numeric_limits<int>::min() + ABS(vrhs32))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<int32_t>(vlhs32 - vrhs32, INT32));
	    
	  case FLOAT:
	    float	vlhsf;
	    float	vrhsf;
	    
	    slhs >> vlhsf;
	    srhs >> vrhsf;
	    if ((vlhsf >= 0 && vrhsf <= 0) || (vlhsf <= 0 && vrhsf >= 0))
	      {	    
		if (ABS(vlhsf) > std::numeric_limits<short>::max() - ABS(vrhsf))
		  throw Error(-1, errors[NOVERFLOW]);
		if (ABS(vlhsf) < std::numeric_limits<short>::min() + ABS(vrhsf))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<float>(vlhsf - vrhsf, FLOAT));
	    
	  case DOUBLE:
	    double	vlhsd;
	    double	vrhsd;
	    
	    slhs >> vlhsd;
	    srhs >> vrhsd;
	    if ((vlhsd >= 0 && vrhsd <= 0) || (vlhsd <= 0 && vrhsd >= 0))
	      {
		if (vlhsd > std::numeric_limits<int>::max() - ABS(vrhsd))
		  throw Error(-1, errors[NOVERFLOW]);
		if (vlhsd < std::numeric_limits<int>::min() + ABS(vrhsd))
		  throw Error(-1, errors[NUNDERFLOW]);
	      }
	    return (new Operand<double>(vlhsd - vrhsd, DOUBLE));
	  };
      }
    catch (Error e)
      {
	std::cerr << e.what() << std::endl;
	std::exit(EXIT_FAILURE);
      }
    return 0;
  }
  
  IOperand		*operator*(const IOperand &rhs) const
  {
    eOperandType       	resType;
    eOperandType       	type1 = getType();
    eOperandType       	type2 = rhs.getType();
    std::istringstream	slhs(toString());
    std::istringstream	srhs(rhs.toString());	

    type1 > type2 ? resType = type1 : resType = type2;
    try
      {
	switch (resType)
	  {
	  case INT8:
	    int	vlhs8;
	    int	vrhs8;
	
	    slhs >> vlhs8;
	    srhs >> vrhs8;
	    if ((vlhs8 >= 0 && vrhs8 >= 0) || (vlhs8 <= 0 && vrhs8 <= 0))
	      {
		if (ABS(vlhs8) > std::numeric_limits<char>::max() / ABS(vrhs8))
		  throw Error(-1, errors[NOVERFLOW]);
	      }
	    else if (ABS(vlhs8) > ABS(std::numeric_limits<char>::min() / vrhs8))
	      throw Error(-1, errors[NUNDERFLOW]);
	    return (new Operand<int8_t>(static_cast<int8_t>(vrhs8 * vlhs8), INT8));
	
	  case INT16:
	    short	vlhs16;
	    short	vrhs16;
	
	    slhs >> vlhs16;
	    srhs >> vrhs16;
	    if ((vlhs16 >= 0 && vrhs16 >= 0) || (vlhs16 <= 0 && vrhs16 <= 0))
	      {
		if (ABS(vlhs16) > std::numeric_limits<short>::max() / ABS(vrhs16))
		  throw Error(-1, errors[NOVERFLOW]);
	      }
	    else if (ABS(vlhs16) > ABS(std::numeric_limits<short>::min() / vrhs16))
	      throw Error(-1, errors[NUNDERFLOW]);
	    return (new Operand<int16_t>(vrhs16 * vlhs16, INT16));
	
	  case INT32:
	    int	vlhs32;
	    int	vrhs32;
	
	    slhs >> vlhs32;
	    srhs >> vrhs32;
	    if ((vlhs32 >= 0 && vrhs32 >= 0) || (vlhs32 <= 0 && vrhs32 <= 0))
	      {
		if (ABS(vlhs32) > std::numeric_limits<int>::max() / ABS(vrhs32))
		  throw Error(-1, errors[NOVERFLOW]);
	      }
	    else if (ABS(vlhs32) > ABS(std::numeric_limits<int>::min() / vrhs32))
	      throw Error(-1, errors[NUNDERFLOW]);
	    return (new Operand<int32_t>(vrhs32 * vlhs32, INT32));

	  case FLOAT:
	    float	vlhsf;
	    float	vrhsf;
	
	    slhs >> vlhsf;
	    srhs >> vrhsf;
	    if ((vlhsf >= 0 && vrhsf >= 0) || (vlhsf <= 0 && vrhsf <= 0))
	      {
		if (ABS(vlhsf) > std::numeric_limits<short>::max() / ABS(vrhsf))
		  throw Error(-1, errors[NOVERFLOW]);
	      }
	    else if (ABS(vlhsf) > ABS(std::numeric_limits<short>::min() / vrhsf))
	      throw Error(-1, errors[NUNDERFLOW]);
	    return (new Operand<float>(vrhsf * vlhsf, FLOAT));
	
	  case DOUBLE:
	    double	vlhsd;
	    double	vrhsd;
	
	    slhs >> vlhsd;
	    srhs >> vrhsd;
	    if ((vlhsd >= 0 && vrhsd >= 0) || (vlhsd <= 0 && vrhsd <= 0))
	      {
		if (ABS(vlhsd) > std::numeric_limits<int>::max() / ABS(vrhsd))
		  throw Error(-1, errors[NOVERFLOW]);
	      }
	    else if (ABS(vlhsd) > ABS(std::numeric_limits<int>::min() / vrhsd))
	      throw Error(-1, errors[NUNDERFLOW]);
	    return (new Operand<double>(vrhsd * vlhsd, DOUBLE));
	  };
      }
    catch (Error e)
      {
	std::cerr << e.what() << std::endl;
	std::exit(EXIT_FAILURE);
      }
    return 0;
  }
  
  IOperand		*operator/(const IOperand &rhs) const
  {
    eOperandType	resType;
    eOperandType	type1 = getType();
    eOperandType	type2 = rhs.getType();
    std::istringstream	slhs(toString());
    std::istringstream	srhs(rhs.toString());
    
    type1 > type2 ? resType = type1 : resType = type2;
    try
      {
	switch (resType)
	  {
	  case INT8:
	    int	vlhs8;
	    int	vrhs8;
	
	    slhs >> vlhs8;
	    srhs >> vrhs8;
	    return (new Operand<int8_t>(static_cast<int8_t>(vlhs8 / vrhs8), INT8));
	
	  case INT16:
	    short	vlhs16;
	    short	vrhs16;
	
	    slhs >> vlhs16;
	    srhs >> vrhs16;
	    return (new Operand<int16_t>(vlhs16 / vrhs16, INT16));
	
	  case INT32:
	    int	vlhs32;
	    int	vrhs32;
	
	    slhs >> vlhs32;
	    srhs >> vrhs32;
	    return (new Operand<int32_t>(vlhs32 / vrhs32, INT32));

	  case FLOAT:
	    float	vlhsf;
	    float	vrhsf;
	
	    slhs >> vlhsf;
	    srhs >> vrhsf;
	    return (new Operand<float>(vlhsf / vrhsf, FLOAT));
	
	  case DOUBLE:
	    double	vlhsd;
	    double	vrhsd;
	
	    slhs >> vlhsd;
	    srhs >> vrhsd;
	    return (new Operand<double>(vlhsd / vrhsd, DOUBLE));
	  };
      }
    catch (Error e)
      {
	std::cerr << e.what() << std::endl;
	std::exit(EXIT_FAILURE);
      }
    return 0;
  }
  
  IOperand		*operator%(const IOperand &rhs) const
  {
    eOperandType	resType;
    eOperandType	type1 = getType();
    eOperandType	type2 = rhs.getType();
    std::istringstream	slhs(toString());
    std::istringstream	srhs(rhs.toString());	

    type1 > type2 ? resType = type1 : resType = type2;
    try
      {
	switch (resType)
	  {
	  case INT8:
	    int	vlhs8;
	    int	vrhs8;
	
	    slhs >> vlhs8;
	    srhs >> vrhs8;
	    return (new Operand<int8_t>(static_cast<int8_t>(vlhs8 % vrhs8), INT8));
	
	  case INT16:
	    short	vlhs16;
	    short	vrhs16;
	
	    slhs >> vlhs16;
	    srhs >> vrhs16;
	    return (new Operand<int16_t>(vlhs16 % vrhs16, INT16));
	
	  case INT32:
	    int	vlhs32;
	    int	vrhs32;
	
	    slhs >> vlhs32;
	    srhs >> vrhs32;
	    return (new Operand<int32_t>(vlhs32 % vrhs32, INT32));

	  case FLOAT:
	    float	vlhsf;
	    float	vrhsf;
	
	    slhs >> vlhsf;
	    srhs >> vrhsf;
	    return (new Operand<float>(fmod(vlhsf, vrhsf), FLOAT));
	
	  case DOUBLE:
	    double	vlhsd;
	    double	vrhsd;
	
	    slhs >> vlhsd;
	    srhs >> vrhsd;
	    return (new Operand<double>(fmod(vlhsd, vrhsd), DOUBLE));
	  };
      }
    catch (Error e)
      {
	std::cerr << e.what() << std::endl;
	std::exit(EXIT_FAILURE);
      }
    return 0;
  }
};

typedef	Operand<int8_t>		Int8;
typedef	Operand<int16_t>	Int16;
typedef	Operand<int32_t>	Int32;
typedef Operand<float>		Float;
typedef	Operand<double>		Double;

#endif
