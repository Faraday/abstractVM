//
// main.cpp for abstract in /home/faraday/projects/abstractVm
//
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
//
// Started on  Mon Feb  9 11:30:00 2015 raphael elkaim
// Last update Fri Feb 20 13:10:20 2015 raphael elkaim
//

#include <map>
#include "Vm.hpp"
#include "abstractVM.hpp"

std::string errors[] =
  {"Syntax error on line ",
   "Unknown instruction on line ",
   "Overflow in operation",
   "Underflow in operation",
   "Can't pop on empty list line ",
   "Error: division/modulo by 0 on line ",
   "Error: no exit statement",
   "Assert failed on line ",
   "Error: can't do operation with less than two operands line ",
   "Error: unknown operand on line ",
   "Error: file doesn't exist"};

int		main(int ac, char **av)
{
  Vm		machine;
  
  machine.run(ac, av);
  return 0;
}
