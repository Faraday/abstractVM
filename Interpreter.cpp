//
// Interpreter.cpp for interpreter in /home/amstuta/rendu/abstractVM
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Feb 11 13:20:54 2015 arthur
// Last update Fri Feb 20 13:14:45 2015 raphael elkaim
//

#include <cstdlib>
#include <fstream>
#include <climits>
#include <iostream>
#include <algorithm>
#include "error.hpp"
#include "operand.hpp"
#include "Interpreter.hpp"

Interpreter::Interpreter(const std::string &fileName):
  fileName(fileName),
  instructions()
{
  sOperands["int8"] = INT8;
  sOperands["int16"] = INT16;
  sOperands["int32"] = INT32;
  sOperands["float"] = FLOAT;
  sOperands["double"] = DOUBLE;
}

Interpreter::~Interpreter()
{
}

bool		Interpreter::onlySpaces(const std::string& line) const
{
  return line.find_first_not_of(' ') == std::string::npos;
}

void		Interpreter::cmdWithArgs(const std::string &line, const size_t pos, const int idx)
{
  IOperand	*res;
  eOperandType	type;
  std::string	cmd, val;
  size_t       	typePos, valPos;

  cmd = line.substr(0, pos);
  try
    {
      if ((typePos = line.find("(")) == std::string::npos || (valPos = line.find(")")) == std::string::npos)
	throw Error(idx, errors[SYNTAX_ERROR]);
    }
  catch (Error e)
    {
      std::cerr << e.what() << std::endl;
      exit(EXIT_FAILURE);
    }
  
  try
    {
      sOperands.at(line.substr(pos + 1, typePos - pos - 1));
    }
  catch (std::out_of_range)
    {
      std::cerr << errors[UNKNOWN_OPERAND] << " " << idx << std::endl;
      exit(EXIT_FAILURE);
    }
  type = sOperands[line.substr(pos + 1, typePos - pos - 1)];
  val = line.substr(typePos + 1, valPos - typePos - 1);
  res = createOperand(type, val);
  (this->stack.*OpStack::operators[cmd])(res);
}

void		Interpreter::cmdSimple(std::string &line, const int idx)
{
  line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
  try
    {
      if (OpStack::operators.find(line) != OpStack::operators.end())
	(this->stack.*OpStack::operators[line])(NULL);
      else
	throw Error(idx, errors[UNKNOWN_INSTRUCTION]);
    }
  catch (Error e)
    {
      std::cerr << e.what() << std::endl;
      exit(EXIT_FAILURE);
    }
}

void		Interpreter::executeLine(std::string &line, const int idx)
{
  size_t	pos;
  std::string	cmd;
  
  if ((pos = line.find(";")) != std::string::npos)
    {
      line = line.substr(0, pos);
      if (pos == 0)
	return;
    }
  if (line.empty() || onlySpaces(line))
    return;

  (line.find(" ") != std::string::npos) ? (pos = line.find(" ")) : (pos = line.size());
  cmd = line.substr(0, pos);
  try
    {
      if (std::find(fctsArgs.begin(), fctsArgs.end(), cmd) != fctsArgs.end())
	cmdWithArgs(line, pos, idx);
      else if (std::find(fctsNoArgs.begin(), fctsNoArgs.end(), cmd) != fctsArgs.end())
	cmdSimple(line, idx);
      else
	throw Error(idx, errors[UNKNOWN_INSTRUCTION]);
    }
  catch (Error e)
    {
      std::cerr << e.what() << std::endl;
      exit(EXIT_FAILURE);
    }
}

void		Interpreter::readFile()
{
  std::ifstream	fd;
  std::string	line;
  int		idx = 0;

  fd.open(this->fileName.c_str());
  try
    {
      if (fd.is_open())
	{
	  while (getline(fd, line))
	    this->instructions.push(line);
	  fd.close();
	  while (!instructions.empty())
	    {
	      executeLine(instructions.front(), idx);
	      instructions.pop();
	      ++idx;
	    }
	}
      else
	throw Error(0, errors[FILE_DOESNT_EXIST]);
    }
  catch (Error e)
    {
      std::cerr << e.what() << std::endl;
      exit(EXIT_FAILURE);
    }
  std::cout << errors[NO_EXIT] << std::endl;
  exit(EXIT_FAILURE);
}

void		Interpreter::readIn()
{
  std::ifstream	fd;
  std::string	line;
  int		idx = 0;

  while (std::getline(std::cin, line))
    {
      if (line.find(";;") != std::string::npos)
	break;
      this->instructions.push(line);
    }
  while (!instructions.empty())
    {
      executeLine(instructions.front(), idx);
      instructions.pop();
      ++idx;
    }
  std::cerr << errors[NO_EXIT] << std::endl;
  std::exit(EXIT_FAILURE);
}

void		Interpreter::chooseEntry()
{
  if (this->fileName.empty())
    readIn();
  else
    readFile();
}

IOperand	*Interpreter::createInt8(const std::string &value)
{
  std::istringstream	s(value);
  int	val;
  
  try
    {
      s.exceptions(std::ios_base::failbit);
      s >> val;
      if (!s.eof())
	throw std::ios_base::failure("not fully numerical");
      if (val > SCHAR_MAX)
	throw Error(0,errors[NOVERFLOW]);
      if (val < SCHAR_MIN)
	throw Error(0,errors[NUNDERFLOW]);
    }
  catch (std::ios_base::failure)
    {
      std::cerr << "Error:invalid/non-numerical value passed to operand" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  catch (Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  return new Int8(val, INT8);
}

IOperand	*Interpreter::createInt16(const std::string &value)
{
  std::istringstream	s(value);
  int16_t	val;

  try
    {
      s.exceptions(std::ios_base::failbit);
      s >> val;
      if (!s.eof())
	throw std::ios_base::failure("not fully numerical");
    }
  catch (std::ios_base::failure)
    {
      std::cerr << "Error:invalid/non-numerical value passed to operand" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  return new Int16(val, INT16);
}

IOperand	*Interpreter::createInt32(const std::string &value)
{
  std::istringstream	s(value);
  int32_t	val;

    try
    {
      s.exceptions(std::ios_base::failbit);
      s >> val;
      if (!s.eof())
	throw std::ios_base::failure("not fully numerical");
    }
    catch (std::ios_base::failure)
    {
      std::cerr << "Error:invalid/non-numerical value passed to operand" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  return new Int32(val, INT32);
}

IOperand	*Interpreter::createFloat(const std::string &value)
{
  std::istringstream	s(value);
  float		val;

    try
    {
      s.exceptions(std::ios_base::failbit);
      s >> val;
      if (!s.eof())
	throw std::ios_base::failure("not fully numerical");
    }
    catch (std::ios_base::failure)
    {
      std::cerr << "Error:invalid/non-numerical value passed to operand" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  return new Float(val, FLOAT);
}

IOperand	*Interpreter::createDouble(const std::string &value)
{
  std::istringstream	s(value);
  double	val;

    try
    {
      s.exceptions(std::ios_base::failbit);
      s >> val;
      if (!s.eof())
	throw std::ios_base::failure("not fully numerical");
    }
    catch (std::ios_base::failure)
    {
      std::cerr << "Error:invalid/non-numerical value passed to operand" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  return new Double(val, DOUBLE);
}

IOperand	*Interpreter::createOperand(eOperandType type, const std::string &value)
{
  return (this->*Interpreter::Ops[type])(value);
}

const std::list<std::string> Interpreter::initList1()
{
  std::list<std::string> ne;
  ne.push_back("push");
  ne.push_back("assert");
  return ne;
}

const std::list<std::string> Interpreter::initList2()
{
  std::list<std::string> ne;
  ne.push_back("pop");
  ne.push_back("dump");
  ne.push_back("add");
  ne.push_back("sub");
  ne.push_back("mul");
  ne.push_back("div");
  ne.push_back("mod");
  ne.push_back("print");
  ne.push_back("exit");
  return ne;
}
const std::list<std::string> Interpreter::fctsArgs = Interpreter::initList1();
const std::list<std::string> Interpreter::fctsNoArgs = Interpreter::initList2();

Interpreter::ops	Interpreter::Ops[5] = {
  &Interpreter::createInt8,
  &Interpreter::createInt16,
  &Interpreter::createInt32,
  &Interpreter::createFloat,
  &Interpreter::createDouble
};
