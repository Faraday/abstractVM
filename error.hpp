//
// error.hpp for error in /home/amstuta/rendu/abstractVM
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Tue Feb 10 11:10:55 2015 arthur
// Last update Mon Feb 16 13:48:31 2015 raphael elkaim
//

#ifndef ERROR_HPP_
#define ERROR_HPP_

#include <string>
#include <exception>

class Error: public std::exception
{
  int		_line;
  std::string	_message;
  // static std::string[] errors;
  
public:
  Error(const int line, const std::string &e);
  virtual ~Error() throw();

  virtual const char	*what() const throw();
};

#endif
