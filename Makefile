##
## Makefile for malloc in /home/elkaim_r/projects/malloc
## 
## Made by raphael elkaim
## Login   <elkaim_r@epitech.net>
## 
## Started on  Tue Jan 27 12:11:45 2015 raphael elkaim
## Last update Tue Feb 17 13:37:34 2015 raphael elkaim
##

CC	 =	g++

RM	 =	rm -f

CXXFLAGS =	-Wextra -Wall -Werror

NAME	 =	avm

SRCS	 =	main.cpp \
		Vm.cpp \
		error.cpp \
		Opstack.cpp \
		Interpreter.cpp \

OBJS	 =	$(SRCS:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
