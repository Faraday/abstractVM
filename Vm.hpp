//
// Vm.hpp for abstract in /home/faraday/projects/abstractVm
//
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
//
// Started on  Mon Feb  9 13:15:08 2015 raphael elkaim
// Last update Fri Feb 13 12:55:42 2015 arthur
//

#ifndef VM_HPP_
# define VM_HPP_

class Vm
{
public:
  Vm();
  ~Vm();
  void	run(int ac, char **av);
};

#endif
