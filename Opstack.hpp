//
// Opstack.hpp for abstract in /home/faraday/projects/abstractVm
//
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
//
// Started on  Wed Feb 11 13:18:09 2015 raphael elkaim
// Last update Mon Feb 16 16:04:13 2015 raphael elkaim
//

#ifndef OPSTACK_HPP_
# define OPSTACK_HPP_

#include <iostream>
#include <cstdlib>
#include <stack>
#include <typeinfo>
#include <map>
#include "abstractVM.hpp"
#include "IOperand.hpp"
#include "operand.hpp"
#include "error.hpp"

class OpStack
{
public:
  OpStack();
  virtual ~OpStack();
  void	add(const IOperand *);
  void	sub(const IOperand *);
  void	mul(const IOperand *);
  void	div(const IOperand *);
  void	mod(const IOperand *);
  void	push(const IOperand *);
  void	pop(const IOperand *);
  void	assert(const IOperand *);
  void	dump(const IOperand *);
  void	print(const IOperand *);
  void	Exit(const IOperand *);
  typedef void (OpStack::*ops)(const IOperand *);
  static std::map<const std::string, ops> operators;
  static std::map<const std::string, ops> init_map();
  
private:
  std::stack<const IOperand *> st;
};

#endif
