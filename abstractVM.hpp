//
// abstractVM.hpp for abstractVM in /home/amstuta/rendu/abstractVM
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Mon Feb  9 12:42:29 2015 arthur
// Last update Thu Feb 19 10:48:44 2015 arthur
//

#ifndef ABSTRACTVM_HPP_
#define ABSTRACTVM_HPP_

#include <map>
#include <string>

#define ABS(a) ((a) < 0 ? (-a) : (a))

typedef enum
  {
    INT8,
    INT16,
    INT32,
    FLOAT,
    DOUBLE
  } eOperandType;

typedef enum
  {
    SYNTAX_ERROR,
    UNKNOWN_INSTRUCTION,
    NOVERFLOW,
    NUNDERFLOW,
    EMPTY_STACK_POP,
    INVALID_DIVISION,
    NO_EXIT,
    ASSERT_FAIL,
    LESS_THAN_TWO_OPERANDS,
    UNKNOWN_OPERAND,
    FILE_DOESNT_EXIST
  } eException;

extern std::string errors[];
extern std::map<std::string, eOperandType> sOperands;

#endif
