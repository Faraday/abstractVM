//
// Vm.cpp for abstract in /home/faraday/projects/abstractVm
//
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
//
// Started on  Mon Feb  9 14:23:27 2015 raphael elkaim
// Last update Fri Feb 13 18:08:15 2015 raphael elkaim
//

#include "Vm.hpp"
#include "Interpreter.hpp"

Vm::Vm()
{
}

Vm::~Vm()
{
}

void	Vm::run(int ac, char **av)
{
  if (ac == 1)
    {
      Interpreter	it;
      it.chooseEntry();
    }
  else
    {
      for (int i(1); i < ac; ++i)
	{
	  Interpreter	it(std::string(av[i]));
	  it.chooseEntry();
	}
    }
}
