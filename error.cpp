//
// error.cpp for abstractvm in /home/amstuta/rendu/abstractVM
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Tue Feb 10 11:23:12 2015 arthur
// Last update Thu Feb 26 11:23:43 2015 raphael elkaim
//

#include <sstream>
#include <iostream>
#include "error.hpp"

Error::Error(const int line, const std::string &e):
  _line(line),
  _message(e)
{
}

Error::~Error() throw()
{
}

const char		*Error::what() const throw()
{
  std::ostringstream	err;

  err << this->_message;
  if (this->_line >= 0)
    err <<  static_cast<int>(this->_line);
  return err.str().c_str();
}
