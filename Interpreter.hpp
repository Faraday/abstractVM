//
// Interpreter.hpp for abstract in /home/faraday/projects/abstractVm
//
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
//
// Started on  Wed Feb 11 13:09:48 2015 raphael elkaim
// Last update Tue Feb 17 14:09:43 2015 raphael elkaim
//

#ifndef INTERPRETER_HPP_
# define INTERPRETER_HPP_

#include <list>
#include <queue>
#include <string>
#include <stdexcept>
#include "Opstack.hpp"

class Interpreter
{
  OpStack				stack;
  std::string				fileName;
  static const std::list<std::string>	fctsArgs;
  static const std::list<std::string>	fctsNoArgs;
  static const std::list<std::string>	initList1();
  static const std::list<std::string>	initList2();
  std::queue<std::string>		instructions;
  std::map<std::string, eOperandType> sOperands;

  typedef IOperand *(Interpreter::*ops)(const std::string &);
  static ops			       	Ops[5];
  
public:
  Interpreter(const std::string &fileName="");
  ~Interpreter();

  void 		chooseEntry();
  bool 		onlySpaces(const std::string &str) const;
  void 		cmdSimple(std::string &line, const int idx);
  void 		executeLine(std::string &line, const int idx);
  IOperand	*createOperand(eOperandType type, const std::string &value);
  void	       	cmdWithArgs(const std::string &line, const size_t pos, const int idx);
  
private:
  void 		readFile();
  void 		readIn();

  IOperand	*createInt8(const std::string &value);
  IOperand	*createInt16(const std::string &value);
  IOperand	*createInt32(const std::string &value);
  IOperand	*createFloat(const std::string &value);
  IOperand	*createDouble(const std::string &value);
};

#endif
