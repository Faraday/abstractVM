//
// Opstack.cpp for abstract in /home/faraday/projects/abstractVm
//
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
//
// Started on  Thu Feb 12 15:08:24 2015 raphael elkaim
// Last update Fri Feb 27 12:06:47 2015 raphael elkaim
//

#include <sstream>
#include "Opstack.hpp"

OpStack::OpStack()
{
}

OpStack::~OpStack()
{
}

void OpStack::add(const IOperand *dum)
{
  static_cast<void>(dum);
  try
    {
      if (st.size() < 2)
	throw Error(0, errors[LESS_THAN_TWO_OPERANDS]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  const IOperand *lhs = st.top();
  st.pop();
  const IOperand *rhs = st.top();
  st.pop();
  st.push(*lhs + *rhs);
}

void OpStack::sub(const IOperand *dum)
{
  static_cast<void>(dum);
  try
    {
      if (st.size() < 2)
	throw Error(0, errors[LESS_THAN_TWO_OPERANDS]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  const IOperand *lhs = st.top();
  st.pop();
  const IOperand *rhs = st.top();
  st.pop();
  st.push(*rhs - *lhs);
}

void OpStack::mul(const IOperand *dum)
{
  static_cast<void>(dum);
  
  try
    {
      if (st.size() < 2)
	throw Error(-1, errors[LESS_THAN_TWO_OPERANDS]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  const IOperand *lhs = st.top();
  st.pop();
  const IOperand *rhs = st.top();
  st.pop();
  st.push(*lhs * *rhs);
}

void OpStack::div(const IOperand *dum)
{
  static_cast<void>(dum);
  
  try
    {
      if (st.size() < 2)
	throw Error(-1, errors[LESS_THAN_TWO_OPERANDS]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  const IOperand *lhs = st.top();
  st.pop();
  const IOperand *rhs = st.top();
  st.pop();
  try
    {
      double test;
      std::istringstream val(lhs->toString());
      val >> test;
      if (!test)
	throw Error(-1, errors[INVALID_DIVISION]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  st.push(*rhs / *lhs);
}

void OpStack::mod(const IOperand *dum)
{
  
  try
    {
      if (st.size() < 2)
	throw Error(-1, errors[LESS_THAN_TWO_OPERANDS]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  static_cast<void>(dum);
  const IOperand *lhs = st.top();
  st.pop();
  const IOperand *rhs = st.top();
  st.pop();
  try
    {
      double test;
      std::istringstream val(lhs->toString());
      val >> test;
      if (!test)
	throw Error(-1, errors[INVALID_DIVISION]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  st.push(*rhs % *lhs);
}

void OpStack::push(const IOperand *ne)
{
  st.push(ne);
}

void OpStack::pop(const IOperand *dum)
{
  static_cast<void>(dum);
  
  try
    {
      if (st.empty())
	throw Error(-1, errors[EMPTY_STACK_POP]);
    }
  catch(Error e)
    {
      std::cerr << e.what() << std::endl;
      std::exit(EXIT_FAILURE);
    }
  st.pop();
}

void OpStack::assert(const IOperand *test)
{
  if (test->toString() != st.top()->toString())
    throw Error(0, errors[ASSERT_FAIL]);
}


void OpStack::print(const IOperand *dum)
{
  static_cast<void>(dum);
  if (typeid(st.top()) != typeid(Int8))
    throw Error(-1, errors[ASSERT_FAIL]);
  std::istringstream str(st.top()->toString());
  int val;
  str >> val;
  std::cout << static_cast<char>(val) << std::endl;
}

void OpStack::dump(const IOperand *dum)
{
  std::stack<const IOperand *> cop(st);
  static_cast<void>(dum);

  while (!cop.empty())
    {
      std::cerr << cop.top()->toString() << std::endl;
      cop.pop();
    }
}

void OpStack::Exit(const IOperand *dum)
{
  static_cast<void>(dum);
  exit(0);
}

std::map<const std::string, OpStack::ops>	OpStack::init_map()
{
  std::map<const std::string, OpStack::ops> new_map;
  
  new_map["add"] = &OpStack::add;
  new_map["sub"] = &OpStack::sub;
  new_map["mul"] = &OpStack::mul;
  new_map["div"] = &OpStack::div;
  new_map["mod"] = &OpStack::mod;
  new_map["push"] = &OpStack::push;
  new_map["pop"] = &OpStack::pop;
  new_map["assert"] = &OpStack::assert;
  new_map["dump"] = &OpStack::dump;
  new_map["print"] = &OpStack::print;
  new_map["exit"] = &OpStack::Exit;

  return new_map;
}


std::map<const std::string, OpStack::ops> OpStack::operators = OpStack::init_map();
